﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStats : MonoBehaviour
{
    public static GlobalStats instance { get; private set; }

    private void Awake()
    {
        instance = this; 
    }

    public float
        attackDmg = 10,
        defense = 50,
        moveSpeed = 5,
        healthPoints = 50;
}
