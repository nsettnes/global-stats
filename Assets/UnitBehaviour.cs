﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class UnitBehaviour : MonoBehaviour
{
    public int controllerId;
    public event Action<STATE_NAME> StateChanged;
    public event Action<UnitBehaviour> UnitDied;
    public Vector3 Position { get; private set; }

    private IEnumerator cr_staterunner;
    private IEnumerator state;
    STATE_NAME stateCurrent = STATE_NAME.idle;
    STATE_NAME stateNext = STATE_NAME.idle;

    public UnitStats Stats { get; private set; }
    public NavMeshAgent Agent { get; private set; }

    public string StateName;

    private void Awake()
    {
        if (Agent == null)
            Agent = GetComponent<NavMeshAgent>();

        if (Stats == null)
            Stats = gameObject.AddComponent<UnitStats>();

        if (state == null)
            ChangeState(stateNext);
    }

    private void Start()
    {
        UnitController.Instance.AddUnitBehaviour(this);
    }

    private void Update()
    {
        Position = transform.position;
        if(stateCurrent != STATE_NAME.die && !Stats.IsAlive)
        {
            ChangeState(STATE_NAME.die);
        }
        //else if (!isStateRunning)
        //    ChangeState(nextState);
    }

    private void ChangeState(STATE_NAME newStateName)
    {
        if (state != null)
            StopCoroutine(state);

        switch (newStateName)
        {
            case STATE_NAME.attack:
                state = CR_Attack();
                break;
            case STATE_NAME.die:
                state = CR_Die();
                break;
            case STATE_NAME.idle:
                state = CR_Idle();
                break;
            case STATE_NAME.move:
                state = CR_Move();
                break;
        }

        //this will make the new state repeat until it ends succesfully making a new state the next state
        stateCurrent = newStateName;
        stateNext = newStateName;
        if (StateChanged != null)
            StateChanged(newStateName);

        RunState();
    }

    void RunState()
    {
        if (cr_staterunner != null)
            StopCoroutine(cr_staterunner);
        cr_staterunner = CR_StateRunner();
        StartCoroutine(cr_staterunner);
    }

    IEnumerator CR_StateRunner()
    {
        yield return StartCoroutine(state);
        ChangeState(stateNext);
    }

    IEnumerator CR_Idle()
    {
        // Essentially: wait for a target, then go to move state
        do
        {
            yield return new WaitForSeconds(1f);
            Stats.Target = UnitController.Instance.GetClosestEnemyUnit(this);
        } while (Stats.Target == null);

            stateNext = STATE_NAME.move;

        yield return null;
    }

    IEnumerator CR_Move()
    {
        if (Agent.isStopped)
            Agent.isStopped = false;

        float timer_eval = 0.3f;
        Vector3 dir = Vector3.zero;
        float dist = 0;

        while (!Helper_MoveShouldStop())
        {
            //dont get a new target every tick, that's overkill
            if (timer_eval > 0)
                timer_eval -= Time.deltaTime;
            else
            {
                Stats.Target = UnitController.Instance.GetClosestEnemyUnit(this);

                dir = (Stats.Target.Position - Position);
                dist = dir.magnitude;
                dir = dir.normalized;

                Agent.SetDestination(Position + dir * (dist - Stats.AttackRange));
                timer_eval = 0.3f;
            }
            yield return null;
        }

        yield return null;
    }

    bool Helper_MoveShouldStop()
    {
        //if the target has become null
        if (!UnitController.Instance.ValidTarget(Stats.Target))
        {
            Agent.isStopped = true;
            stateNext = STATE_NAME.idle;
            return true;
        }

        //if we are close enough to the target to attack, change states
        if (Vector3.Distance(Position, Stats.Target.Position) < Stats.AttackRange + Agent.stoppingDistance)
        {
            Agent.isStopped = true;
            stateNext = STATE_NAME.attack;
            return true;
        }

        return false;
    }

    IEnumerator CR_Attack()
    {
        bool isTargetOutOfRange = false;
        bool isTargetInvalid = false;

        //wait for the attack to commence (animation-wise)
        float attack_timer = Stats.AttackAnimStart;
        while (attack_timer > 0 && !(isTargetInvalid || isTargetOutOfRange))
        {
            yield return null;

            attack_timer -= Time.deltaTime;

            Helper_AttackAssignTargetVars(ref isTargetInvalid, ref isTargetOutOfRange);
        }

        //damage the other unit midway through the animation
        attack_timer -= Time.deltaTime;
        Stats.Target.Stats.DamageUnit(Stats.AttackDamage);
        attack_timer = Stats.AttackAnimEnd;

        //wait for the attack to complete (animation-wise)
        yield return new WaitForSeconds(attack_timer);
        Helper_AttackAssignTargetVars(ref isTargetInvalid, ref isTargetOutOfRange);

        if(isTargetInvalid)
            stateNext = STATE_NAME.idle;
        else if (isTargetOutOfRange)
            stateNext = STATE_NAME.move;
    }

    void Helper_AttackAssignTargetVars(ref bool isTargetInvalid, ref bool isTargetOutOfRange)
    {
        isTargetInvalid = !UnitController.Instance.ValidTarget(Stats.Target);
        isTargetOutOfRange = Vector3.Distance(Position, Stats.Target.Position) > Stats.AttackRange + Agent.stoppingDistance;
    }

    IEnumerator CR_Die()
    {
        UnitDied(this);

        this.GetComponent<Collider>().enabled = false;
        Agent.enabled = false;
        
        //wait for the animation to have played, then delete this gameobject
        yield return new WaitForSeconds(8);
        Destroy(this.gameObject);

        yield return null;
    }
}

public enum STATE_NAME { move, attack, idle, die }