﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMaterialController : MonoBehaviour
{
    public static UnitMaterialController Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;
    }

    public List<Material> GoblinMaterials = new List<Material>();

    public Material GetGoblinMaterialForTeam(int teamIndex)
    {
        return GoblinMaterials[teamIndex];
    }
}
