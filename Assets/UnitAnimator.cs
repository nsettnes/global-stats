﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimator : MonoBehaviour
{
    public Animator anim;

    void Awake()
    {
        Debug.Log("UnitAnimator Awake");
        anim = GetComponentInChildren<Animator>();
        GetComponent<UnitBehaviour>().StateChanged += UnitAnimator_StateChanged;
	}

    private void UnitAnimator_StateChanged(STATE_NAME stateName)
    {
        Debug.Log("UnitAnimator: new state: " + stateName.ToString());
        switch (stateName)
        {
            case STATE_NAME.attack:
                anim.SetInteger("moving", 3);
                break;
            case STATE_NAME.die:
                anim.SetTrigger("dead1");
                break;
            case STATE_NAME.idle:
                anim.SetInteger("moving", 1);
                break;
            case STATE_NAME.move:
                anim.SetInteger("moving", 2);
                break;
        }
    }
}
