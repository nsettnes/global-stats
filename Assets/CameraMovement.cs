﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float camMoveSpeed = 5; 
    Vector3 moveHelper, moveVector;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        moveHelper = Vector3.zero;
        moveVector = Vector3.zero;

        if (Input.GetKey(KeyCode.S))
        {
            moveHelper = (-transform.forward);
            moveHelper.y = 0;
            moveVector += moveHelper;
        }
        if (Input.GetKey(KeyCode.W))
        {
            moveHelper = (transform.forward);
            moveHelper.y = 0;
            moveVector += moveHelper;
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveHelper = (-transform.right);
            moveHelper.y = 0;
            moveVector += moveHelper;
        }
        if (Input.GetKey(KeyCode.D))
        {
            moveHelper = (transform.right);
            moveHelper.y = 0;
            moveVector += moveHelper;
        }

        moveVector = moveVector.normalized;

        transform.position += moveVector * camMoveSpeed * Time.deltaTime;
    }
}
