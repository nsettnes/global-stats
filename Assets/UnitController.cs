﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    public static UnitController Instance
    {
        get;
        private set;
    }

    private List<UnitBehaviour> AllUnits = new List<UnitBehaviour>();

    private void Awake()
    {
        Instance = this;
    }

    public void AddUnitBehaviour(UnitBehaviour ub)
    {
        AllUnits.Add(ub);
        ub.UnitDied += Ub_UnitDied;
    }

    private void Ub_UnitDied(UnitBehaviour ub)
    {
        AllUnits.Remove(ub);
    }

    public UnitBehaviour GetClosestEnemyUnit(UnitBehaviour ub)
    {
        return AllUnits.Where(y => y.controllerId != ub.controllerId).OrderBy(x => Vector3.Distance(ub.Position, x.Position)).FirstOrDefault();
    }

    public bool ValidTarget(UnitBehaviour ub)
    {
        return ub != null && ub.Stats.IsAlive && AllUnits.Contains(ub);
    }

    public List<UnitBehaviour> GetTeamUnits(int teamId)
    {
        return AllUnits.Where(y => y.controllerId == teamId).ToList();
    }
}
