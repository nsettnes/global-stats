﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitColoring : MonoBehaviour
{
    public Renderer GoblinRenderer;

	void Start ()
    {
        GoblinRenderer.sharedMaterial = UnitMaterialController.Instance.GetGoblinMaterialForTeam(GetComponent<UnitBehaviour>().controllerId);
	}
}
