﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITeamUnitCounter : MonoBehaviour
{
    public List<Text> Texts = new List<Text>();

    void Update () {
        int idx = 0;
        foreach (Text t in Texts)
        {
            t.text = "Team " + idx + ": " + UnitController.Instance.GetTeamUnits(idx).Count.ToString();
                idx++;
        }
	}
}
