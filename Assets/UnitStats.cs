﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : MonoBehaviour
{
    public UnitBehaviour Target;

    public bool IsAlive
    {
        get
        {
            return HealthCurrent >= 0;
        }
    }

    public float HealthMax = 100f;
    public float HealthCurrent = 100f;

    public float AttackDamage = 15f;
    public float AttackRange = 2f;
    public float AttackAnimStart = 0.4f;
    public float AttackAnimEnd = 0.45f;

    public void DamageUnit(float attackStrength)
    {
        HealthCurrent -= attackStrength;
    }
}
